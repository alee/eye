/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


/**********************************************************************
              				Header Files
 ***********************************************************************/
#include "eye.h"

extern EyeVideoPlayer *defaultVideoPlayer;
extern ClutterActor *pVideoPlayerStage ;
static void video_player_free_pkgdatadir_text(gchar *msg_text);
/**********************************************************************
              				enum Declaration Starts.
 ***********************************************************************/
/* Thumb Roller enum */
enum
{
	VIDEO_THUMB_PATH,
	VIDEO_AV_OBJECT,
	VIDEO_AV_ACTOR,
	VIDEO_SHOW,
	VIDEO_THUMB_LAST
};
/* Sort Roller enum */
enum
{
	SORT_ROLLER_COLUMN_NAME,
	SORT_ROLLER_COLUMN_ICON,
	SORT_ROLLER_COLUMN_LABEL,
	SORT_ROLLER_COLUMN_TYPE,
	SORT_ROLLER_COLUMN_LAST
};

/* drawer model columns */
enum
{
	DRAWER_COLUMN_NAME,
	DRAWER_COLUMN_ICON,
	DRAWER_COLUMN_TOOLTIP_TEXT,
	DRAWER_COLUMN_REACTIVE,
	DRAWER_COLUMN_LAST
};

/* List Roller enum */
enum
{
	PROP_ICON,
	PROP_TOP_TEXT,
	PROP_BELOW_LEFT_TEXT,
	PROP_BELOW_RIGHT_TEXT,
	PROP_LAST
};

/* songs model columns */
enum
{
	META_ROLLER_COLUMN_ID,
	META_ROLLER_COLUMN_NUM,
	META_ROLLER_COLUMN_ICON,
	META_ROLLER_COLUMN_LABEL,
	META_ROLLER_COLUMN_LAST
};

enum 
{
	BOTTOM_BAR_ONE_ACTIVE,
	BOTTOM_BAR_ONE_INACTIVE,
	BOTTOM_BAR_ONE_TEXTURE_NAME,
	BOTTOM_BAR_TWO_ACTIVE,
	BOTTOM_BAR_TWO_INACTIVE,
	BOTTOM_BAR_TWO_TEXTURE_NAME,
	BOTTOM_BAR_THREE_ACTIVE,
	BOTTOM_BAR_THREE_INACTIVE,
	BOTTOM_BAR_THREE_TEXTURE_NAME,
	BOTTOM_BAR_FOUR,
	BOTTOM_BAR_LAST
};

enum
{
	INFO_ROLLER_HEADER_LEFT_ICON_TEXT,
	INFO_ROLLER_HEADER_MID_TEXT,
	INFO_ROLLER_HEADER_RIGHT_ICON_TEXT,
	INFO_ROLLER_HEADER_NONE
};

/**********************************************************************
              				enum Declaration Ends.
 ***********************************************************************/

/**********************************************************************
              			Function Prototypes Start
 ***********************************************************************/

static void create_video_player_thumb_view(EyeVideoPlayer *pVideoPlayer);
static void init_new_hash_tables(EyeVideoPlayer *pVideoPlayer);
static void set_initial_configuration(EyeVideoPlayer *pVideoPlayer);
static void initialize_video_player_views(EyeVideoPlayer *pVideoPlayer);
static ThornburyModel *create_thumb_roller_model (void);
static GHashTable *create_thumb_roller_signals (const gchar *pWidgetName,
    EyeVideoPlayer *pVideoPlayer);
static gboolean set_sort_roller_item_to_focus(gpointer pUserData);
static ThornburyModel *create_video_player_sort_roller_model (void);
static void sort_roller_add_attributes (const gchar *pWidgetName,
    EyeVideoPlayer *pVideoPlayer);
static ThornburyModel *create_video_player_views_drawer_model (void);
static GHashTable *create_views_drawer_signals (const gchar *pWidgetName,
    EyeVideoPlayer *pVideoPlayer);
static ThornburyModel *create_video_player_thumb_context_drawer_model (void);
static GHashTable *create_context_drawer_signals (const gchar *pWidgetName,
    EyeVideoPlayer *pVideoPlayer);
static void create_video_player_list_view(EyeVideoPlayer *pVideoPlayer);
static ThornburyModel *create_list_roller_model (void);
static GHashTable *create_list_roller_signals (const gchar *pWidgetName,
    EyeVideoPlayer *pVideoPlayer);

static void create_video_player_detail_view(EyeVideoPlayer *pVideoPlayer);

static void create_video_player_full_screen_view(EyeVideoPlayer *pVideoPlayer);
/**********************************************************************
              			Function Prototypes End
 ***********************************************************************/

gboolean eye_update_list_roller(gpointer pUserData)
{
	gchar **pThumbNailList = (gchar **)pUserData;
    gchar *pThumbNailPath = pThumbNailList[defaultVideoPlayer->priv->rowNumber];
	const gchar *pVideoPath = g_ptr_array_index(defaultVideoPlayer->priv->pVideoUriList, defaultVideoPlayer->priv->rowNumber);
	GFile *pFilePointer = g_file_new_for_path (pVideoPath);
	/* FIXME: Add error handling */
	GrassmoorMediaInfo* MedStructVideoInfo = grassmoor_tracker_get_media_file_info (defaultVideoPlayer->priv->tracker, pVideoPath, GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO, NULL);
    gchar *pFileBaseName = g_file_get_basename(pFilePointer);
	gint duration = 0;
    gint iMinutes,iSeconds;
    gchar *totalDuration;
    gchar *pMinutes,*pSeconds;
    gchar *pMovieTitle;
    gchar *pMovieDate;
    ClutterContent *pContent;

    pFileBaseName = g_strdelimit(pFileBaseName,"%20",' ');

    if(MedStructVideoInfo)
        {
                duration = grassmoor_media_info_get_duration (MedStructVideoInfo);
                iMinutes = duration / 60;
                iSeconds = duration % 60;
                pMinutes = g_strdup_printf("%d",iMinutes);
                pSeconds = g_strdup_printf("%d",iSeconds);
                totalDuration = g_strconcat(pMinutes," min ",pSeconds," sec",NULL);
                if (grassmoor_media_info_get_year (MedStructVideoInfo))
                  pMovieDate = g_strdup (grassmoor_media_info_get_year (MedStructVideoInfo));
                else
                  pMovieDate = g_strdup("Unknown");
        }
        else
        {
                duration = 0;
                iMinutes = duration / 60;
                iSeconds = duration % 60;
                pMinutes = g_strdup_printf("%d",iMinutes);
                pSeconds = g_strdup_printf("%d",iSeconds);
                totalDuration = g_strconcat(pMinutes," min ",pSeconds," sec",NULL);
                pMovieDate = g_strdup("Unknown");

        }
        if(pFileBaseName)
        {
                pMovieTitle = g_strdup(pFileBaseName);
        }
        else
        {
                pMovieTitle = g_strdup("Unknown");
        }
	if(pThumbNailPath != NULL)
        {
        	GFile *pFile = g_file_new_for_commandline_arg (pThumbNailPath);
                if(pFile != NULL)
                {
                        if(g_file_query_exists(pFile,NULL))
                	{
                                EYE_VIDEOPLAYER_DEBUG("\n eye_populate_list_roller_items : FILE EXISTS \n ");
                                pContent = video_player_create_texture_content(pThumbNailPath, 162, 162);
                        }
                        else
                        {
                                gchar *pIconPath;
                                EYE_VIDEOPLAYER_DEBUG("\n eye_populate_list_roller_items : FILE DOESN'T EXISTS \n ");
                                pIconPath = g_strconcat (PKG_DATA_DIRECTORY, "/IconBig_Video.png", NULL);
                                pContent = video_player_create_texture_content(pIconPath, 162, 162);
                                video_player_free_pkgdatadir_text(pIconPath);
                        }
                }
        }
        if(pContent != NULL)
	{
                thornbury_model_append (defaultVideoPlayer->priv->pListRollerModel, PROP_ICON,pContent,PROP_TOP_TEXT,pMovieTitle,PROP_BELOW_LEFT_TEXT,pMovieDate,PROP_BELOW_RIGHT_TEXT,totalDuration,-1);
		defaultVideoPlayer->priv->rowNumber++;
	}
        video_player_free_pkgdatadir_text(totalDuration);
        video_player_free_pkgdatadir_text(pMinutes);
        video_player_free_pkgdatadir_text(pSeconds);
        video_player_free_pkgdatadir_text(pMovieTitle);
        video_player_free_pkgdatadir_text(pMovieDate);
	if(defaultVideoPlayer->priv->rowNumber < defaultVideoPlayer->priv->pVideoUriList->len)
        {
        	return G_SOURCE_CONTINUE;
        }
        else
        {
        	return FALSE;
        }
	return FALSE;
}


/*********************************************************************************************
 * Function: eye_update_thumb_list
 * Description: updates the thumb roller and list roller 
 * Parameters:  gpointer
 * Return:   void
 ********************************************************************************************/
gboolean eye_update_thumb_list(gpointer pUserData)
{
        gchar **pThumbNailList = (gchar **)pUserData;
        gchar *pThumbNailPath = pThumbNailList[defaultVideoPlayer->priv->inThumbsToUpdate];
        GFile *pFile = g_file_new_for_commandline_arg (pThumbNailPath);
        if(pFile != NULL)
        {
                if(g_file_query_exists(pFile,NULL))
                {
                        //g_print("\n eye_populate_thumb_roller_items : FILE EXISTS \n ");
                        ClutterContent *pContent = video_player_create_texture_content(pThumbNailPath, 162, 162);
                        if(pContent != NULL)
                        {
                                GValue pValue = { 0, };

                                g_value_init (&pValue, G_TYPE_OBJECT);
                                //FIXME:thornbury_model_insert_value
                                g_value_set_object(&pValue, pContent);
                                thornbury_model_insert_value(defaultVideoPlayer->priv->pThumbRollerModel,defaultVideoPlayer->priv->inThumbsToUpdate,VIDEO_THUMB_PATH,&pValue);
                                defaultVideoPlayer->priv->inThumbsToUpdate++;
                        }
                        if(defaultVideoPlayer->priv->inThumbsToUpdate < defaultVideoPlayer->priv->pVideoUriList->len)
                        {
                                return G_SOURCE_CONTINUE;
                        }
                        else
                        {
                                return FALSE;
                        }
                }
		else
                {
                                gchar *pBgPath = g_strconcat(PKG_DATA_DIRECTORY,"/IconBig_Video.png",NULL);
                                ClutterContent *pContent = video_player_create_texture_content(pBgPath, 162, 162);

                                if(pContent != NULL)
                                {
                                        GValue pValue = { 0, };
                                        g_value_init (&pValue, G_TYPE_OBJECT);
                                        //FIXME:thornbury_model_insert_value
                                        g_value_set_object(&pValue, pContent);
                                        thornbury_model_insert_value(defaultVideoPlayer->priv->pThumbRollerModel,defaultVideoPlayer->priv->inThumbsToUpdate,VIDEO_THUMB_PATH,&pValue);
                                        defaultVideoPlayer->priv->inThumbsToUpdate++;
                                }
                                if(defaultVideoPlayer->priv->inThumbsToUpdate < defaultVideoPlayer->priv->pVideoUriList->len)
                                {
                                        return G_SOURCE_CONTINUE;
                                }
                                else
                                {
                                        return FALSE;
                                }

                }
        }
	return FALSE;
}

/*********************************************************************************************
 * Function: create_thumb_roller_model   
 * Description: To create the Thumb roller model
 * Parameters:  -
 * Return:   ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *
create_thumb_roller_model (void)
{
	ThornburyModel *model = NULL;

	EYE_VIDEOPLAYER_DEBUG("\n create_thumb_roller_model \n ");

	model = (ThornburyModel *)thornbury_list_model_new (VIDEO_THUMB_LAST,G_TYPE_OBJECT, NULL,G_TYPE_OBJECT,NULL, G_TYPE_OBJECT,NULL, G_TYPE_BOOLEAN,NULL,-1);

	return model;
}

/*********************************************************************************************
 * Function: eye_init_ui   
 * Description: To initialise the VideoPlayer UI and variables
 * Parameters:  EyeVideoPlayer
 * Return:   void
 ********************************************************************************************/
void eye_init_ui(EyeVideoPlayer *pVideoPlayer)
{
	EYE_VIDEOPLAYER_DEBUG("\n eye_init_ui \n ");
	init_new_hash_tables(pVideoPlayer);
	set_initial_configuration(pVideoPlayer);
	initialize_video_player_views(pVideoPlayer);
}


static void video_player_free_pkgdatadir_text(gchar *msg_text)
{
	if(msg_text)
	{
     		g_free(msg_text);
     		msg_text=NULL;
	}
}

/*********************************************************************************************
 * Function: initialize_video_player_views   
 * Description: To initialise the VideoPlayer views
 * Parameters:  EyeVideoPlayer
 * Return:   void
 ********************************************************************************************/
static void initialize_video_player_views(EyeVideoPlayer *pVideoPlayer)
{
	EyeVideoPlayerPrivate *priv;
	gchar *path;
	ThornburyViewManagerAppData *pAppData;

	g_return_if_fail (EYE_IS_VIDEO_PLAYER (pVideoPlayer));
	EYE_VIDEOPLAYER_DEBUG("\n initialize_video_player_views \n ");

	priv = pVideoPlayer->priv;
	path = g_strconcat(PKG_DATA_DIRECTORY,"/",NULL);
	pAppData = g_new0(ThornburyViewManagerAppData,1);
	pAppData->app= CLUTTER_ACTOR(pVideoPlayer);
	pAppData->default_view="ThumbView";
	pAppData->viewspath = path;
	pAppData->app_name = priv->EYE_APP_NAME;
	pAppData->stage = (ClutterStage*)pVideoPlayerStage ;
	pAppData->pAppBackFunc = NULL ;
	pAppData->pScreenShotPath = NULL ;
	priv->pThornburyViewManager = thornbury_view_manager_new(pAppData);

	thornbury_build_all_views(priv->pThornburyViewManager);
	/* view switch callback */
	g_signal_connect(priv->pThornburyViewManager,"view_switch_end",G_CALLBACK(eye_view_switched), pVideoPlayer);
	g_signal_connect(priv->pThornburyViewManager, "view_switch_begin", G_CALLBACK(eye_view_switch_begin_cb), pVideoPlayer);
	if(path != NULL)
	{	
		g_free(path);
		path = NULL;
	}
	create_video_player_thumb_view(pVideoPlayer);
	create_video_player_list_view(pVideoPlayer);
	create_video_player_detail_view(pVideoPlayer);
	create_video_player_full_screen_view(pVideoPlayer);
	extract_video_info_from_meta_tracker(pVideoPlayer);
	if(thornbury_model_get_n_rows(
			pVideoPlayer->priv->pThumbRollerModel) > 0)
	{
		thornbury_set_property(pVideoPlayer->priv->pThornburyViewManager,
		THUMB_VIEW_VIDEO_THUMB_ROLLER, "focused-row", 0, NULL);
	}
}

/*********************************************************************************************
 * Function:    create_list_context_drawer_model
 * Description: 
 * Parameters:  void
 * Return:      ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *
create_detail_context_drawer_model (void)
{
	ThornburyModel *model = NULL;
	gchar *pIconPath;

	model = (ThornburyModel *)thornbury_list_model_new (DRAWER_COLUMN_LAST,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_BOOLEAN, NULL,
			-1);
	pIconPath = g_strconcat (PKG_DATA_DIRECTORY, "/icon_search.png", NULL);
	thornbury_model_append (model, DRAWER_COLUMN_NAME, SEARCH,
			DRAWER_COLUMN_ICON, pIconPath,
			DRAWER_COLUMN_TOOLTIP_TEXT, "SEARCH",
			DRAWER_COLUMN_REACTIVE, TRUE,
			-1);
	video_player_free_pkgdatadir_text(pIconPath);
	pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_playlists.png",NULL);
	thornbury_model_append (model, DRAWER_COLUMN_NAME, TO_PLAYLIST,
			DRAWER_COLUMN_ICON, pIconPath,
			DRAWER_COLUMN_TOOLTIP_TEXT, "TO PLAYLIST",
			DRAWER_COLUMN_REACTIVE, TRUE,
			-1);
	video_player_free_pkgdatadir_text(pIconPath);
	
	pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_playlists.png",NULL);
	thornbury_model_append (model, DRAWER_COLUMN_NAME, FAVOURITE,
			DRAWER_COLUMN_ICON, pIconPath,
			DRAWER_COLUMN_TOOLTIP_TEXT, "FAVOURITE",
			DRAWER_COLUMN_REACTIVE, TRUE,
			-1);
	video_player_free_pkgdatadir_text(pIconPath);
	
	pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_related.png",NULL);
	thornbury_model_append (model, DRAWER_COLUMN_NAME, RELATED,
			DRAWER_COLUMN_ICON, pIconPath,
			DRAWER_COLUMN_TOOLTIP_TEXT, "RELATED",
			DRAWER_COLUMN_REACTIVE, TRUE,
			-1);
	video_player_free_pkgdatadir_text(pIconPath);
	
	pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_released.png",NULL);
	thornbury_model_append (model,  DRAWER_COLUMN_NAME, RECENT,
			DRAWER_COLUMN_ICON, pIconPath,
			DRAWER_COLUMN_TOOLTIP_TEXT, "RECENT",
			DRAWER_COLUMN_REACTIVE, TRUE,
			-1);
	video_player_free_pkgdatadir_text(pIconPath);
	
	pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_trash.png",NULL);
	thornbury_model_append (model, DRAWER_COLUMN_NAME, DELETE,
			DRAWER_COLUMN_ICON, pIconPath,
			DRAWER_COLUMN_TOOLTIP_TEXT, "DELETE",
			DRAWER_COLUMN_REACTIVE, TRUE,
			-1);
	video_player_free_pkgdatadir_text(pIconPath);
	return model;
}

/*********************************************************************************************
 * Function: create_meta_roller_model   
 * Description: 
 * Parameters:  
 * Return:   ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *
create_meta_roller_model (void)
{
	ThornburyModel *model = NULL;
	gchar *pIconPath;

	model = (ThornburyModel *)thornbury_list_model_new (META_ROLLER_COLUMN_LAST,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			-1);
	pIconPath = g_strconcat (PKG_DATA_DIRECTORY, "/icon_duration.png", NULL);
	thornbury_model_append (model, META_ROLLER_COLUMN_ICON, pIconPath,
			META_ROLLER_COLUMN_LABEL,"Unknown",
			-1);
	video_player_free_pkgdatadir_text(pIconPath);
	
	pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_released.png",NULL);
	thornbury_model_append (model, META_ROLLER_COLUMN_ICON, pIconPath,
			META_ROLLER_COLUMN_LABEL,"Unknown",
			-1);
	video_player_free_pkgdatadir_text(pIconPath);
	
	pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_resolution.png",NULL);
	thornbury_model_append (model, META_ROLLER_COLUMN_ICON, pIconPath,
			META_ROLLER_COLUMN_LABEL,"Unknown",
			-1);
	video_player_free_pkgdatadir_text(pIconPath);
	return model;
}

/*********************************************************************************************
 * Function: create_bottom_bar_model   
 * Description: 
 * Parameters:  
 * Return:   ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *
create_bottom_bar_model (void)
{
	ThornburyModel *model = NULL;
	gchar *pShuffleIconPathAC = g_strconcat (PKG_DATA_DIRECTORY, "/icon_shuffle_AC.png", NULL);
	gchar *pShuffleIconPathIN = g_strconcat (PKG_DATA_DIRECTORY, "/icon_shuffle_IN.png", NULL);
	gchar *pRepeatIconPathAC = g_strconcat (PKG_DATA_DIRECTORY, "/icon_repeat_AC.png", NULL);
	gchar *pRepeatIconPathIN = g_strconcat (PKG_DATA_DIRECTORY, "/icon_repeat_IN.png", NULL);

	model = (ThornburyModel *)thornbury_list_model_new (BOTTOM_BAR_LAST,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			-1);

	thornbury_model_append(model, 
			BOTTOM_BAR_ONE_ACTIVE, pShuffleIconPathAC,
			BOTTOM_BAR_ONE_INACTIVE, pShuffleIconPathIN,
			BOTTOM_BAR_ONE_TEXTURE_NAME, SHUFFLE,
			BOTTOM_BAR_TWO_ACTIVE, pRepeatIconPathAC,
			BOTTOM_BAR_TWO_INACTIVE, pRepeatIconPathIN,
			BOTTOM_BAR_TWO_TEXTURE_NAME, REPEAT,
			BOTTOM_BAR_THREE_ACTIVE, NULL,
			BOTTOM_BAR_THREE_INACTIVE, NULL,
			BOTTOM_BAR_THREE_TEXTURE_NAME, NULL,	
			BOTTOM_BAR_FOUR, g_strdup("0:00"),
			-1);
	video_player_free_pkgdatadir_text(pShuffleIconPathAC);
	video_player_free_pkgdatadir_text(pShuffleIconPathIN);
	video_player_free_pkgdatadir_text(pRepeatIconPathAC);
	video_player_free_pkgdatadir_text(pRepeatIconPathIN);
	return model;
}

/*********************************************************************************************
 * Function:    create_progress_bar_signals
 * Description: signal hash for progress bar
 * Parameters:  pWidgetName* , pVideoPlayer*
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *create_progress_bar_signals (const gchar *pWidgetName,
    EyeVideoPlayer *pVideoPlayer)
{
	GHashTable *signals = g_hash_table_new (g_direct_hash, g_str_equal);
	GList *lstsignals = NULL;
	ThornburyViewManagerSignalData *sig_data1 = g_new0 (ThornburyViewManagerSignalData, 1);
	ThornburyViewManagerSignalData *sig_data2 = g_new0 (ThornburyViewManagerSignalData, 1);
	ThornburyViewManagerSignalData *sig_data3 = g_new0 (ThornburyViewManagerSignalData, 1);
	ThornburyViewManagerSignalData *sig_data4 = g_new0 (ThornburyViewManagerSignalData, 1);
	ThornburyViewManagerSignalData *sig_data5 = g_new0 (ThornburyViewManagerSignalData, 1);

	sig_data1->signalname = "play-requested",
			sig_data1->func = G_CALLBACK(progress_bar_play_requested_cb),
			sig_data1->data = pVideoPlayer;
	/* append signal to list */
	lstsignals = g_list_append(lstsignals, sig_data1);

	sig_data2->signalname = "pause-requested",
			sig_data2->func = G_CALLBACK(progress_bar_pause_requested_cb),
			sig_data2->data = pVideoPlayer;
	/* append signal to list */
	lstsignals = g_list_append(lstsignals, sig_data2);

	sig_data3->signalname = "seek-start",
			sig_data3->func = G_CALLBACK(progress_bar_seek_started_cb),
			sig_data3->data = pVideoPlayer;
	/* append signal to list */
	lstsignals = g_list_append(lstsignals, sig_data3);

	sig_data4->signalname = "seek-end",
			sig_data4->func = G_CALLBACK(progress_bar_seek_end_cb),
			sig_data4->data = pVideoPlayer;

	/* append signal to list */
	lstsignals = g_list_append(lstsignals, sig_data4);

	sig_data5->signalname = "progress-updated",
			sig_data5->func = G_CALLBACK(progress_bar_seek_updated_cb),
			sig_data5->data = pVideoPlayer;

	/* append signal to list */
	lstsignals = g_list_append(lstsignals, sig_data5);


	/* add list to hash */
	g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);

	return signals;
}

/*********************************************************************************************
 * Function:    create_bottom_bar_signals
 * Description: signal hash for bottom bar
 * Parameters:  pWidgetName* , pVideoPlayer*
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *create_bottom_bar_signals (const gchar *pWidgetName,
    EyeVideoPlayer *pVideoPlayer)
{
	GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
	GList *lstsignals = NULL;
	ThornburyViewManagerSignalData *sig_data1 = g_new0 (ThornburyViewManagerSignalData, 1);
	ThornburyViewManagerSignalData *sig_data2 = g_new0 (ThornburyViewManagerSignalData, 1);

	sig_data1->signalname = "action-press",
			sig_data1->func = G_CALLBACK(bottom_bar_pressed),
			sig_data1->data = pVideoPlayer;
	/* append signal to list */
	lstsignals = g_list_append(lstsignals, sig_data1);

	sig_data2->signalname = "action-release",
			sig_data2->func = G_CALLBACK(bottom_bar_released),
			sig_data2->data = pVideoPlayer;
	/* append signal to list */
	lstsignals = g_list_append(lstsignals, sig_data2);

	/* add list to hash */
	g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);

	return signals;
}

/*********************************************************************************************
 * Function: create_info_header_model   
 * Description: 
 * Parameters:  
 * Return:   ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *
create_info_header_model (void)
{
	ThornburyModel *model = NULL;	
	gchar *pIconPath = g_strconcat (PKG_DATA_DIRECTORY, "/icon_videoTV_AC.png", NULL);

	model = (ThornburyModel *)thornbury_list_model_new (INFO_ROLLER_HEADER_NONE,
			G_TYPE_POINTER, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			-1);

	thornbury_model_append (model,
			INFO_ROLLER_HEADER_LEFT_ICON_TEXT,pIconPath,
			INFO_ROLLER_HEADER_MID_TEXT, "Video Player",
			INFO_ROLLER_HEADER_RIGHT_ICON_TEXT, NULL,
			-1);

	return model;
}



/*********************************************************************************************
 * Function:    create_full_screen_left_context_drawer_model
 * Description: 
 * Parameters:  void
 * Return:      ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *
create_full_screen_left_context_drawer_model (void)
{
	ThornburyModel *model = NULL;
	gchar *pIconPath = g_strconcat (PKG_DATA_DIRECTORY, "/icon_zoomback.png", NULL);

	model = (ThornburyModel *)thornbury_list_model_new (DRAWER_COLUMN_LAST,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_BOOLEAN, NULL,
			-1);

	thornbury_model_append (model, DRAWER_COLUMN_NAME, VIEW_SWITCH,
			DRAWER_COLUMN_ICON, pIconPath,
			DRAWER_COLUMN_TOOLTIP_TEXT, " ",
			DRAWER_COLUMN_REACTIVE, TRUE,
			-1);
	video_player_free_pkgdatadir_text(pIconPath);

	return model;
}

/*********************************************************************************************
 * Function:    create_full_screen_right_context_drawer_model
 * Description: 
 * Parameters:  void
 * Return:      ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *
create_full_screen_right_context_drawer_model (void)
{
	ThornburyModel *model = NULL;
	gchar *pIconPath = g_strconcat (PKG_DATA_DIRECTORY, "/icon_video_aspect_IN.png", NULL);

	model = (ThornburyModel *)thornbury_list_model_new (DRAWER_COLUMN_LAST,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_BOOLEAN, NULL,
			-1);

	thornbury_model_append (model, DRAWER_COLUMN_NAME, ASPECT_RATIO,
			DRAWER_COLUMN_ICON, pIconPath,
			DRAWER_COLUMN_TOOLTIP_TEXT, " ",
			DRAWER_COLUMN_REACTIVE, TRUE,
			-1);
	video_player_free_pkgdatadir_text(pIconPath);

	return model;
}

/*********************************************************************************************
 * Function:    create_context_drawer_signals
 * Description: signal hash for context drawer
 * Parameters:  none
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *
create_full_screen_left_context_drawer_signals (const gchar *pWidgetName,
    EyeVideoPlayer *pVideoPlayer)
{
	GHashTable *signals = g_hash_table_new (g_direct_hash, g_str_equal);
	GList *lstsignals = NULL;
	ThornburyViewManagerSignalData *sig_data = g_new0 (ThornburyViewManagerSignalData, 1);

	EYE_VIDEOPLAYER_DEBUG ("\n create_context_drawer_signals \n ");

	sig_data->signalname = "drawer-button-released";
	sig_data->func = G_CALLBACK(eye_full_screen_left_context_drawer_button_released_cb);
	sig_data->data = pVideoPlayer;

	lstsignals = g_list_append(lstsignals,sig_data);

	g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);


	return signals;
}



/*********************************************************************************************
 * Function:    create_context_drawer_signals
 * Description: signal hash for context drawer
 * Parameters:  none
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *
create_full_screen_right_context_drawer_signals (const gchar *pWidgetName,
    EyeVideoPlayer *pVideoPlayer)
{
	GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
	GList *lstsignals = NULL;
	ThornburyViewManagerSignalData *sig_data = g_new0(ThornburyViewManagerSignalData,1);

	EYE_VIDEOPLAYER_DEBUG ("\n create_context_drawer_signals \n ");

	sig_data->signalname = "drawer-button-released";
	sig_data->func = G_CALLBACK(eye_full_screen_right_context_drawer_button_released_cb);
	sig_data->data = pVideoPlayer;

	lstsignals = g_list_append(lstsignals,sig_data);

	g_hash_table_insert(signals, (gchar *) pWidgetName, lstsignals);

	return signals;
}


/*******************************************************************
 *Function: create_video_player_full_screen_view
 *Description: Used to create the VideoPlayer Full screen view
 *Paramters: EyeVideoPlayer element
 *Return Value: void
 *******************************************************************/
static void create_video_player_full_screen_view(EyeVideoPlayer *pVideoPlayer)
{
	EyeVideoPlayerPrivate *priv;
	ThornburyModel *pLeftContextModel;
	ThornburyModel *pRightContextModel;
	GHashTable *pLeftContextSignalHash;
	GHashTable *pRightContextSignalHash;
	GHashTable *anim;
	ThornburyViewManagerViewSwitchAnimData *animData2;

	g_return_if_fail (EYE_IS_VIDEO_PLAYER (pVideoPlayer));
	EYE_VIDEOPLAYER_DEBUG("\n create_video_player_detail_view \n ");
	//FIXME:Commented for 24-May-2013 release.
	priv = pVideoPlayer->priv;
	anim = g_hash_table_new (g_str_hash, g_str_equal);
	animData2 = g_new0(ThornburyViewManagerViewSwitchAnimData,1);
	animData2->view_name = "FullScreenView";
	animData2->exit_effect = "exit_full_screen";
	animData2->entry_effect = "enter_full_screen";
	g_hash_table_insert (anim, (gchar *) "FullScreenView", animData2);

	thornbury_set_view_switch_animation(priv->pThornburyViewManager,anim);

	pLeftContextModel = create_full_screen_left_context_drawer_model ();
	g_hash_table_insert (priv->pFullScreenModelHash,
		(gchar *) FULL_SCREEN_VIEW_SWITCH_DRAWER, pLeftContextModel);

	pRightContextModel = create_full_screen_right_context_drawer_model ();
	g_hash_table_insert (priv->pFullScreenModelHash,
		(gchar *) FULL_SCREEN_ASPECT_RATIO_DRAWER, pRightContextModel);


	thornbury_set_widgets_models(priv->pThornburyViewManager, priv->pFullScreenModelHash);

	pLeftContextSignalHash = create_full_screen_left_context_drawer_signals (FULL_SCREEN_VIEW_SWITCH_DRAWER, pVideoPlayer);
	pRightContextSignalHash = create_full_screen_right_context_drawer_signals (FULL_SCREEN_ASPECT_RATIO_DRAWER, pVideoPlayer);

	thornbury_set_widgets_controllers(priv->pThornburyViewManager, pLeftContextSignalHash);
	thornbury_set_widgets_controllers(priv->pThornburyViewManager, pRightContextSignalHash);
}

/*******************************************************************
 *Function: create_video_player_list_view
 *Description: Used to create the VideoPlayer list view
 *Paramters: EyeVideoPlayer element
 *Return Value: void
 *******************************************************************/
static void create_video_player_detail_view(EyeVideoPlayer *pVideoPlayer)
{
	EyeVideoPlayerPrivate *priv;
	ThornburyModel *pContextModel;
	GHashTable *pContextSignalHash;
	GHashTable *pProgressBarSignalHash;
	GHashTable *pBottomBarSignalHash;

	g_return_if_fail (EYE_IS_VIDEO_PLAYER (pVideoPlayer));
	EYE_VIDEOPLAYER_DEBUG("\n create_video_player_detail_view \n ");

	priv = pVideoPlayer->priv;
	/* add context drawer model */
	pContextModel = create_detail_context_drawer_model ();
	g_hash_table_insert (priv->pDetailModelHash,
		(gchar *) DETAIL_CONTEXT_DRAWER, pContextModel);

	/* add meta roller model */
	priv->pMetaRollerModel = create_meta_roller_model();
	g_hash_table_insert (priv->pDetailModelHash,
		(gchar *) DETAIL_META_ROLLER, priv->pMetaRollerModel);

	priv->pBottomBarModel = create_bottom_bar_model();
	g_hash_table_insert (priv->pDetailModelHash,
		(gchar *) DETAIL_BOTTOMBAR, priv->pBottomBarModel);

	/* set model hash to view manager */
	thornbury_set_widgets_models(priv->pThornburyViewManager, priv->pDetailModelHash);

	/* add signals */
	pContextSignalHash = create_context_drawer_signals (DETAIL_CONTEXT_DRAWER, pVideoPlayer);
	pProgressBarSignalHash = create_progress_bar_signals (DETAIL_PROGRESSBAR, pVideoPlayer);
	pBottomBarSignalHash = create_bottom_bar_signals (DETAIL_BOTTOMBAR, pVideoPlayer);

	/* set signals to view manager */
	thornbury_set_widgets_controllers(priv->pThornburyViewManager, pContextSignalHash);
	thornbury_set_widgets_controllers(priv->pThornburyViewManager, pProgressBarSignalHash);
	thornbury_set_widgets_controllers(priv->pThornburyViewManager, pBottomBarSignalHash);


	/* add attributes to meta roller */
	thornbury_set_property(priv->pThornburyViewManager, DETAIL_META_ROLLER,
			"icon", META_ROLLER_COLUMN_ICON,
			"text", META_ROLLER_COLUMN_LABEL,
			NULL);

	priv->pInfoHeaderModel = create_info_header_model();
	thornbury_set_property(priv->pThornburyViewManager, DETAIL_INFO_ROLLER,
			"model", priv->pInfoHeaderModel,
			NULL);

}

/*******************************************************************
 *Function: set_initial_configuration
 *Description: Set all initial config params
 *Paramters: EyeVideoPlayer
 *Return Value: void
 *******************************************************************/
static void set_initial_configuration(EyeVideoPlayer *pVideoPlayer)
{
	EYE_VIDEOPLAYER_DEBUG("\n set_initial_configuration \n ");
	pVideoPlayer->priv->fileToUpdate = 0;
	pVideoPlayer->priv->prevPlayingFile = -1;
	pVideoPlayer->priv->player = NULL;
	pVideoPlayer->priv->seekStart = FALSE;
	pVideoPlayer->priv->inDisconnect = 1;
	pVideoPlayer->priv->isShuffle = FALSE;
	pVideoPlayer->priv->isRepeat = FALSE;
	pVideoPlayer->priv->rowNumber = 0;
	pVideoPlayer->priv->playingDuration = 0.0;
	pVideoPlayer->priv->progressBarPlayState = TRUE;
	pVideoPlayer->priv->pCurrentView = 0 ;
	pVideoPlayer->priv->pExtraThumbs = 0;
	pVideoPlayer->priv->inRequestCount = 0;
	pVideoPlayer->priv->bRemoveState = FALSE;
	pVideoPlayer->priv->inThumbsToUpdate = 0;
}

/*******************************************************************
 *Function: init_new_hash_tables
 *Description: initialise new hash tables
 *Paramters: EyeVideoPlayer element
 *Return Value: void
 *******************************************************************/
static void init_new_hash_tables(EyeVideoPlayer *pVideoPlayer)
{
	EyeVideoPlayerPrivate *priv = pVideoPlayer->priv;

	EYE_VIDEOPLAYER_DEBUG ("\n init_new_hash_tables \n ");

	priv->pThumbModelHash = g_hash_table_new(g_str_hash,g_str_equal);
	priv->pListModelHash = g_hash_table_new(g_str_hash,g_str_equal);
	priv->pDetailModelHash = g_hash_table_new(g_str_hash,g_str_equal);
	priv->pFullScreenModelHash = g_hash_table_new(g_str_hash,g_str_equal);
	priv->pExitStateHash = g_hash_table_new(g_str_hash,g_str_equal);
}

/*******************************************************************
 *Function: create_video_player_list_view
 *Description: Used to create the VideoPlayer list view
 *Paramters: EyeVideoPlayer element
 *Return Value: void
 *******************************************************************/
static void create_video_player_list_view(EyeVideoPlayer *pVideoPlayer)
{
	EyeVideoPlayerPrivate *priv = pVideoPlayer->priv;
	GHashTable *pListSignalHash;

	EYE_VIDEOPLAYER_DEBUG ("\n create_video_player_list_view \n ");

	priv->pListRollerModel = create_list_roller_model();
	g_hash_table_insert (priv->pListModelHash,
		(gchar *) LIST_VIEW_LIST_ROLLER, priv->pListRollerModel);

	pListSignalHash = create_list_roller_signals(LIST_VIEW_LIST_ROLLER, pVideoPlayer);

	thornbury_set_property(priv->pThornburyViewManager, LIST_VIEW_LIST_ROLLER,"icon", PROP_ICON,NULL);
	thornbury_set_property(priv->pThornburyViewManager, LIST_VIEW_LIST_ROLLER,"top-text", PROP_TOP_TEXT,NULL);
	thornbury_set_property(priv->pThornburyViewManager, LIST_VIEW_LIST_ROLLER,"below-left-text", PROP_BELOW_LEFT_TEXT,NULL);
	thornbury_set_property(priv->pThornburyViewManager, LIST_VIEW_LIST_ROLLER,"below-right-text", PROP_BELOW_RIGHT_TEXT,NULL);

	thornbury_set_widgets_models(priv->pThornburyViewManager, priv->pListModelHash);
	thornbury_set_widgets_controllers(priv->pThornburyViewManager, pListSignalHash);
}

/*******************************************************************
 *Function: create_list_roller_model
 *Description: Used to create the list roller model
 *Paramters: -
 *Return Value: ThornburyModel
 *******************************************************************/
static ThornburyModel *
create_list_roller_model (void)
{
	ThornburyModel *video_model;
	video_model = (ThornburyModel *)thornbury_list_model_new(PROP_LAST, G_TYPE_OBJECT, NULL,G_TYPE_STRING, NULL,G_TYPE_STRING, NULL,G_TYPE_STRING, NULL,-1);
	return video_model;
}

/*******************************************************************
 *Function: create_list_roller_signals
 *Description: Used to create the roller signals
 *Paramters: pWidgetName , EyeVideoPlayer
 *Return Value: GHashTable
 *******************************************************************/
static GHashTable *
create_list_roller_signals (const gchar *pWidgetName,
    EyeVideoPlayer *pVideoPlayer)
{
	GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
	GList *lstsignals = NULL;

	ThornburyViewManagerSignalData *sig_data1 = g_new0(ThornburyViewManagerSignalData,1);
	sig_data1->signalname = "roller-item-activated",
			sig_data1->func = G_CALLBACK(eye_list_roller_item_activated_cb),
			sig_data1->data = pVideoPlayer;

	/* append signal to list */
	lstsignals = g_list_append(lstsignals, sig_data1);

	g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);

	return signals;
}

static GHashTable *
create_animations (void)
{

	GHashTable *animation = g_hash_table_new(g_str_hash,g_str_equal);
	ThornburyViewManagerViewSwitchAnimData *animData = g_new0(ThornburyViewManagerViewSwitchAnimData,1);
	animData->view_name = "ThumbView";
	animData->exit_effect = "cover_to_detail";
	animData->entry_effect = "detail_to_cover";
	g_hash_table_insert (animation, (gchar *) "ThumbView", animData);
	return animation;

}




/*******************************************************************
 *Function: create_video_player_thumb_view
 *Description: Used to create the thumb view
 *Paramters: EyeVideoPlayer
 *Return Value: void
 *******************************************************************/
static void create_video_player_thumb_view(EyeVideoPlayer *pVideoPlayer)
{
	EyeVideoPlayerPrivate *priv = pVideoPlayer->priv;
	GHashTable *pThumbSignalHash;
	GHashTable *pViewsDrawerSignalHash;
	GHashTable *pContextDrawerSignalHash;
	GHashTable *anim;

	EYE_VIDEOPLAYER_DEBUG ("\n create_video_player_thumb_view \n ");

	priv->pThumbRollerModel = create_thumb_roller_model();
	g_hash_table_insert (priv->pThumbModelHash,
		(gchar *) THUMB_VIEW_VIDEO_THUMB_ROLLER, priv->pThumbRollerModel);

	priv->pSortRollerModel = create_video_player_sort_roller_model();
	g_hash_table_insert (priv->pThumbModelHash,
		(gchar *) THUMB_VIEW_SORT_ROLLER, priv->pSortRollerModel);

	priv->pViewsDrawerModel = create_video_player_views_drawer_model();
	g_hash_table_insert (priv->pThumbModelHash,
		(gchar *) THUMB_VIEWS_DRAWER, priv->pViewsDrawerModel);

	/* add context drawer model */
	priv->pContextDrawerModel = create_video_player_thumb_context_drawer_model();
	g_hash_table_insert (priv->pThumbModelHash,
		(gchar *) THUMB_CONTEXT_DRAWER, priv->pContextDrawerModel);


	pThumbSignalHash = create_thumb_roller_signals (THUMB_VIEW_VIDEO_THUMB_ROLLER, pVideoPlayer);
	pViewsDrawerSignalHash = create_views_drawer_signals (THUMB_VIEWS_DRAWER, pVideoPlayer);
	pContextDrawerSignalHash = create_context_drawer_signals (THUMB_CONTEXT_DRAWER, pVideoPlayer);

	thornbury_set_property(priv->pThornburyViewManager, THUMB_VIEW_VIDEO_THUMB_ROLLER,"show", VIDEO_SHOW,NULL);
	thornbury_set_property(priv->pThornburyViewManager, THUMB_VIEW_VIDEO_THUMB_ROLLER,"av-object", VIDEO_AV_OBJECT,NULL);
	thornbury_set_property(priv->pThornburyViewManager, THUMB_VIEW_VIDEO_THUMB_ROLLER,"av-actor", VIDEO_AV_ACTOR,NULL);
	thornbury_set_property(priv->pThornburyViewManager, THUMB_VIEW_VIDEO_THUMB_ROLLER,"icon-data", VIDEO_THUMB_PATH,NULL);

	thornbury_set_widgets_models(priv->pThornburyViewManager, priv->pThumbModelHash);

	thornbury_set_widgets_controllers(priv->pThornburyViewManager, pThumbSignalHash);
	thornbury_set_widgets_controllers(priv->pThornburyViewManager, pViewsDrawerSignalHash);
	thornbury_set_widgets_controllers(priv->pThornburyViewManager, pContextDrawerSignalHash);


	sort_roller_add_attributes(THUMB_VIEW_SORT_ROLLER, pVideoPlayer);

	anim = create_animations();
	thornbury_set_view_switch_animation(priv->pThornburyViewManager,anim);
}

/*********************************************************************************************
 * Function:    create_context_drawer_signals
 * Description: signal hash for context drawer
 * Parameters:  none
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *
create_context_drawer_signals (const gchar *pWidgetName,
    EyeVideoPlayer *pVideoPlayer)
{
	GHashTable *signals = g_hash_table_new (g_direct_hash, g_str_equal);
	GList *lstsignals = NULL;
	ThornburyViewManagerSignalData *sig_data = g_new0 (ThornburyViewManagerSignalData,1);

	EYE_VIDEOPLAYER_DEBUG ("\n create_context_drawer_signals \n ");

	sig_data->signalname = "drawer-button-released";
	sig_data->func = G_CALLBACK(eye_thumb_context_drawer_button_released_cb);
	sig_data->data = pVideoPlayer;

	lstsignals = g_list_append(lstsignals,sig_data);

	g_hash_table_insert(signals, (gchar *) pWidgetName, lstsignals);

	return signals;
}

/*********************************************************************************************
 * Function:    create_video_player_thumb_context_drawer_model
 * Description: Used to create the model for context drawer
 * Parameters:  void
 * Return:      ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *
create_video_player_thumb_context_drawer_model (void)
{
	ThornburyModel *model = NULL;
	gchar *pIconPath;

	model = (ThornburyModel *)thornbury_list_model_new (DRAWER_COLUMN_LAST,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_BOOLEAN, NULL,
			-1);

	pIconPath = g_strconcat (PKG_DATA_DIRECTORY, "/icon_search.png", NULL);
	thornbury_model_append (model, DRAWER_COLUMN_NAME, SEARCH,
			DRAWER_COLUMN_ICON, pIconPath,
			DRAWER_COLUMN_TOOLTIP_TEXT, "SEARCH",
			DRAWER_COLUMN_REACTIVE, TRUE,
			-1);
	video_player_free_pkgdatadir_text(pIconPath);
	return model;
}

/*********************************************************************************************
 * Function:    create_views_drawer_model
 * Description: Used to create the views drawer model
 * Parameters:  
 * Return:      ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *
create_video_player_views_drawer_model (void)
{
	ThornburyModel *model = NULL;
	gchar *pIconPath;

	model = (ThornburyModel *)thornbury_list_model_new (DRAWER_COLUMN_LAST,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_BOOLEAN, NULL,
			-1);

	pIconPath = g_strconcat (PKG_DATA_DIRECTORY, "/icon_view_details.png", NULL);
	thornbury_model_append (model,  DRAWER_COLUMN_NAME, "DETAIL-VIEW",
			DRAWER_COLUMN_ICON, pIconPath,
			DRAWER_COLUMN_TOOLTIP_TEXT, "DETAIL",
			DRAWER_COLUMN_REACTIVE, TRUE,
			-1);
	video_player_free_pkgdatadir_text(pIconPath);

	pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_view_listwiththumbs_IN.png",NULL);
	thornbury_model_append (model, DRAWER_COLUMN_NAME, "LIST-VIEW",
			DRAWER_COLUMN_ICON, pIconPath,
			DRAWER_COLUMN_TOOLTIP_TEXT, "LIST",
			DRAWER_COLUMN_REACTIVE, TRUE,
			-1);
	video_player_free_pkgdatadir_text(pIconPath);
	
	pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_view_thumbs.png",NULL);
	thornbury_model_append (model, DRAWER_COLUMN_NAME, "THUMB-VIEW",
			DRAWER_COLUMN_ICON, pIconPath,
			DRAWER_COLUMN_TOOLTIP_TEXT, "THUMB",
			DRAWER_COLUMN_REACTIVE, TRUE,
			-1);
	video_player_free_pkgdatadir_text(pIconPath);

	return model;
}

/*********************************************************************************************
 * Function:    create_views_drawer_signals
 * Description: signal hash for views drawer
 * Parameters:  pWidgetName,EyeVideoPlayer
 * Return:      GHashTable *
 ********************************************************************************************/
static GHashTable *
create_views_drawer_signals (const gchar *pWidgetName,
    EyeVideoPlayer *pVideoPlayer)
{
	GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
	GList *lstsignals = NULL;

	ThornburyViewManagerSignalData *sig_data = g_new0(ThornburyViewManagerSignalData,1);
	sig_data->signalname = "drawer-button-released";
	sig_data->func = G_CALLBACK(eye_views_drawer_button_released_cb);
	sig_data->data = pVideoPlayer;

	lstsignals = g_list_append(lstsignals,sig_data);

	g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);

	return signals;
}



/*********************************************************************************************
 * Function:    sort_roller_add_attributtes
 * Description: add attributes to sort roller
 * Parameters:  pWidgetName,EyeVideoPlayer
 * Return:      void
 ********************************************************************************************/
static void sort_roller_add_attributes (const gchar *pWidgetName,
    EyeVideoPlayer *pVideoPlayer)
{
	if(NULL != pWidgetName)
	{
		EyeVideoPlayerPrivate *priv = pVideoPlayer->priv;
		thornbury_set_property(priv->pThornburyViewManager, pWidgetName,
				"label", SORT_ROLLER_COLUMN_LABEL, 
				"icon", SORT_ROLLER_COLUMN_ICON, 
				"column-type", SORT_ROLLER_COLUMN_TYPE, 
				NULL);

	}
}

/*********************************************************************************************
 * Function: set_sort_roller_item__to_focus
 * Description:  To bring first item to focus on sort roller
 * Parameters:  pUserData
 * Return:   gboolean(handled or not)
 ********************************************************************************************/
static gboolean set_sort_roller_item_to_focus(gpointer pUserData)
{
	EyeVideoPlayer *pVideoPlayer = EYE_VIDEO_PLAYER (pUserData);
	EyeVideoPlayerPrivate *priv = pVideoPlayer->priv;
	thornbury_set_property(priv->pThornburyViewManager, THUMB_VIEW_SORT_ROLLER, "focused-row", 1, NULL);
	thornbury_set_property(priv->pThornburyViewManager, THUMB_VIEW_VIDEO_THUMB_ROLLER, "focused-row", 0, NULL);

	return FALSE;
}

/*********************************************************************************************
 * Function: create_sort_roller_model   
 * Description: Used to create the model for the sort roller
 * Parameters:  -
 * Return:   ThornburyModel*
 ********************************************************************************************/
static ThornburyModel *
create_video_player_sort_roller_model (void)
{
	ThornburyModel *model = (ThornburyModel *)thornbury_list_model_new(SORT_ROLLER_COLUMN_LAST,
			G_TYPE_STRING, NULL, 
			COGL_TYPE_HANDLE, NULL, 
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL, -1);

	/* alphabet from A to Z */	
	int inInnerLoop;
	inInnerLoop = 65;
	while (inInnerLoop <= 90)
	{

		thornbury_model_append(model, 
				SORT_ROLLER_COLUMN_NAME, g_strdup_printf("item number %c", inInnerLoop), 
				SORT_ROLLER_COLUMN_LABEL, g_strdup_printf("%c", inInnerLoop), 
				SORT_ROLLER_COLUMN_TYPE, g_strdup("alphabet"),
				-1);
		inInnerLoop++;
	}
	return model;
}

/*********************************************************************************************
 * Function: create_thumb_roller_signals   
 * Description: Used to add the signals for the thumb roller
 * Parameters:  pWidgetName,pVideoPlayer
 * Return:   GHashTable*
 ********************************************************************************************/
static GHashTable *
create_thumb_roller_signals (const gchar *pWidgetName,
    EyeVideoPlayer *pVideoPlayer)
{
	GHashTable *signals = g_hash_table_new(g_direct_hash,g_str_equal);
	GList *lstsignals = NULL;

	ThornburyViewManagerSignalData *sig_data1 = g_new0(ThornburyViewManagerSignalData,1);
	sig_data1->signalname = "roller-item-activated",
			sig_data1->func = G_CALLBACK(eye_thumb_roller_item_activated_cb),
			sig_data1->data = pVideoPlayer;

	/* append signal to list */
	lstsignals = g_list_append(lstsignals, sig_data1);

	g_hash_table_insert (signals, (gchar *) pWidgetName, lstsignals);

	return signals;
}


void update_video_player_progress_and_bottom_bar(gdouble progress)
{
	gint mins = 0;
	gint secs = 0;
	gchar *displaySecs = NULL;
	gchar *displayMins = NULL;
	gchar *newTime = NULL ;
	gint timer = 0;
	gdouble dura;
	GValue value = { 0 };

	thornbury_set_property(defaultVideoPlayer->priv->pThornburyViewManager, DETAIL_PROGRESSBAR, "current-duration",progress, NULL);
	g_object_get(defaultVideoPlayer->priv->player,"duration",&dura,NULL);

	timer = progress * dura;
	if(timer >= 60)
	{
		mins = timer/60;
		secs = timer%60;
	}
	else
	{
		mins = 0;
		secs = timer;
	}

	if(mins < 10)
		displayMins = g_strdup_printf("%s""%d", "0", mins);
	else
		displayMins = g_strdup_printf("%d", mins);
	if(secs < 10)
		displaySecs = g_strdup_printf("%s""%d", "0", secs);
	else
		displaySecs = g_strdup_printf("%d", secs);

	newTime = g_strjoin(":", displayMins, displaySecs, NULL);

	g_value_init(&value, G_TYPE_STRING);
	g_value_set_string(&value, newTime);

	/* update bottom bar model with new time */
	if(NULL != defaultVideoPlayer->priv->pBottomBarModel)
		thornbury_model_insert_value(defaultVideoPlayer->priv->pBottomBarModel, 0, BOTTOM_BAR_FOUR, &value);

	if(NULL != displayMins)
	{
		g_free(displayMins);
		displayMins = NULL;
	}
	if(NULL != displaySecs)
	{
		g_free(displaySecs);
		displaySecs = NULL;
	}
}

ClutterContent *video_player_create_texture_content(gchar *pFilePath, gint width, gint height)
{
	GFile *pFile = g_file_new_for_commandline_arg (pFilePath);
	ClutterContent *pTexture = NULL;
	gchar *pUri = NULL;
	GError *pErr = NULL;
	GdkPixbuf *pixbuf = NULL;

	if (pFile == NULL)
	{
		return NULL;
	}
	if (g_file_has_uri_scheme (pFile, "file"))
	{
		pUri = g_file_get_path (pFile);
		pixbuf = gdk_pixbuf_new_from_file_at_scale (pUri, width, height, FALSE, &pErr);
		if (pixbuf == NULL || pErr != NULL)
		{
			g_free (pUri);
			return NULL;
		}
		pTexture = clutter_image_new ();
		clutter_image_set_data(CLUTTER_IMAGE (pTexture),
				gdk_pixbuf_get_pixels (pixbuf),
				gdk_pixbuf_get_has_alpha (pixbuf)? COGL_PIXEL_FORMAT_RGBA_8888 : COGL_PIXEL_FORMAT_RGB_888,
						gdk_pixbuf_get_width (pixbuf),
						gdk_pixbuf_get_height (pixbuf),
						gdk_pixbuf_get_rowstride (pixbuf),
						&pErr);
		if (pErr != NULL)
		{
			g_free (pUri);
			return NULL;
		}
	}
	return pTexture;
}

static void set_status_bar_clb( GObject *source_object,GAsyncResult *res,gpointer user_data)
{
	GError *error = NULL;
	mildenhall_statusbar_call_set_status_bar_info_finish(defaultVideoPlayer->priv->status_bar_proxy,res,&error);
}

void update_detail_view_header(gchar *pFileName)
{
	if(pFileName != NULL)
	{
		GValue pName = { 0, };
		GFile *pFilePointer = g_file_new_for_path((const char*)pFileName);
		gchar *pFileBaseName = g_file_get_basename(pFilePointer);
		gchar *pIconPath;

		g_value_init(&pName, G_TYPE_STRING);
		pFileBaseName = g_strdelimit(pFileBaseName,"%20",' ');
		g_value_set_string(&pName, pFileBaseName);
		defaultVideoPlayer->priv->pHeaderText = g_strdup(pFileName);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pInfoHeaderModel,0,INFO_ROLLER_HEADER_MID_TEXT,&pName);
		pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_videoTV_AC.png",NULL);
		mildenhall_statusbar_call_set_status_bar_info (defaultVideoPlayer->priv->status_bar_proxy,defaultVideoPlayer->priv->EYE_APP_NAME, "NULL", pIconPath,g_strdup(pFileBaseName),NULL,set_status_bar_clb,NULL);
		video_player_free_pkgdatadir_text(pIconPath);
	}
}

void
add_items_to_thumb_roller (void)
{
	ClutterActor *videoActor;

	gchar *pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/IconBig_Video.png",NULL);
	ClutterContent *pContent = video_player_create_texture_content(pIconPath, 162, 162);
	video_player_free_pkgdatadir_text(pIconPath);
	defaultVideoPlayer->priv->player = g_object_new(GRASSMOOR_TYPE_AV_PLAYER, "player-mode", GRASSMOOR_AV_PLAYER_MODE_VIDEO, NULL);

	
	g_object_get(defaultVideoPlayer->priv->player, "video-actor", &videoActor, NULL);

	if(pContent != NULL)
	{
		thornbury_model_append (defaultVideoPlayer->priv->pThumbRollerModel, VIDEO_THUMB_PATH,pContent,VIDEO_AV_OBJECT,defaultVideoPlayer->priv->player, VIDEO_AV_ACTOR, videoActor, VIDEO_SHOW,FALSE,-1);
	}
}

/*********************************************************************************************
 * Function:    add_remove_thumbs_divisible_by_4
 * Description: extra thumbs to be added/ removed
 * Parameters:  Pointer to the audio player element, bAddThumbs
 * Return:      void
 ********************************************************************************************/
void add_remove_thumbs_divisible_by_4(gboolean bAddThumbs)
{
        if(defaultVideoPlayer->priv->pExtraThumbs > 0)
        {
                gint inCount = defaultVideoPlayer->priv->pExtraThumbs;
                for(; inCount > 0; inCount--)
                {
                        if(bAddThumbs)
                        {
				thornbury_model_append (defaultVideoPlayer->priv->pThumbRollerModel, VIDEO_THUMB_PATH,NULL,VIDEO_AV_OBJECT,NULL,VIDEO_AV_ACTOR, NULL, VIDEO_SHOW,FALSE,-1);
				defaultVideoPlayer->priv->bRemoveState = TRUE;
                        }
                        else 
                        {
                                /* remove dummy items */
                                thornbury_model_remove(defaultVideoPlayer->priv->pThumbRollerModel, thornbury_model_get_n_rows(defaultVideoPlayer->priv->pThumbRollerModel) - 1 );
				defaultVideoPlayer->priv->bRemoveState = FALSE;
                        }
                }
        }
}

void update_detail_meta_roller(GrassmoorMediaInfo *MedStruct)
{
	GValue pName = { 0, };
	gint min,sec;
	gchar *displaySecs = NULL;
	gchar *displayMins = NULL;
	gchar *newTime = NULL ;

	g_value_init(&pName, G_TYPE_STRING);
	if (grassmoor_media_info_get_year (MedStruct))
	{
    	  if(strlen (grassmoor_media_info_get_year (MedStruct)))
		g_value_set_string (&pName, grassmoor_media_info_get_year (MedStruct));
	  else
		g_value_set_string(&pName, "Unknown");
	}
	else
	  g_value_set_string(&pName, "Unknown");

	thornbury_model_insert_value(defaultVideoPlayer->priv->pMetaRollerModel,1,META_ROLLER_COLUMN_LABEL,&pName);
	if (grassmoor_media_info_get_duration (MedStruct) >= 60)
	{
		min = grassmoor_media_info_get_duration (MedStruct) / 60;
		sec = grassmoor_media_info_get_duration (MedStruct) % 60;
	}
	else
	{
		min = 0;
		sec = grassmoor_media_info_get_duration (MedStruct);
	}

	if(min < 10)
		displayMins = g_strdup_printf("%s""%d", "0", min);
	else
		displayMins = g_strdup_printf("%d", min);
	if(sec < 10)
		displaySecs = g_strdup_printf("%s""%d", "0", sec);
	else
		displaySecs = g_strdup_printf("%d", sec);

	newTime = g_strjoin(":", displayMins, displaySecs, NULL);

	EYE_VIDEOPLAYER_DEBUG("\n Meta roller = %s",newTime);
	g_value_set_string(&pName, newTime);
	thornbury_model_insert_value(defaultVideoPlayer->priv->pMetaRollerModel,0,META_ROLLER_COLUMN_LABEL,&pName);
	thornbury_set_property(defaultVideoPlayer->priv->pThornburyViewManager, DETAIL_META_ROLLER, "focused-row", 1, NULL);
	video_player_free_pkgdatadir_text(displaySecs);
	video_player_free_pkgdatadir_text(displayMins);
}


void update_views_drawer(gint inView)
{
	GValue pColName = { 0, };
	GValue pColIcon = { 0, };
	GValue pColToolTip = { 0, };
	gchar *pIconPath = NULL;

	g_value_init (&pColName, G_TYPE_STRING);
	g_value_init (&pColIcon, G_TYPE_STRING);
	g_value_init (&pColToolTip, G_TYPE_STRING);

	if (inView == THUMB_VIEW)
	{
		pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_view_details.png",NULL);
		g_value_set_string(&pColName, "DETAIL-VIEW");
		g_value_set_string(&pColIcon, pIconPath);
		g_value_set_string(&pColToolTip, "DETAIL");
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,0,DRAWER_COLUMN_NAME,&pColName);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,0,DRAWER_COLUMN_ICON,&pColIcon);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,0,DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);
		video_player_free_pkgdatadir_text(pIconPath);
		
		pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_view_listwiththumbs_IN.png",NULL);
		g_value_set_string(&pColName, "LIST-VIEW");
		g_value_set_string(&pColIcon, pIconPath);
		g_value_set_string(&pColToolTip, "LIST");
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,1,DRAWER_COLUMN_NAME,&pColName);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,1,DRAWER_COLUMN_ICON,&pColIcon);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,1,DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);
		video_player_free_pkgdatadir_text(pIconPath);

		pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_view_thumbs.png",NULL);
		g_value_set_string(&pColName, "THUMB-VIEW");
		g_value_set_string(&pColIcon, pIconPath);
		g_value_set_string(&pColToolTip, "THUMB");
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,2,DRAWER_COLUMN_NAME,&pColName);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,2,DRAWER_COLUMN_ICON,&pColIcon);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,2,DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);
		video_player_free_pkgdatadir_text(pIconPath);
	}
	else if(inView == DETAIL_VIEW)
	{

		pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_view_thumbs.png",NULL);
		g_value_set_string(&pColName, "THUMB-VIEW");
		g_value_set_string(&pColIcon, pIconPath);
		g_value_set_string(&pColToolTip, "THUMB");
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,0,DRAWER_COLUMN_NAME,&pColName);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,0,DRAWER_COLUMN_ICON,&pColIcon);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,0,DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);
		video_player_free_pkgdatadir_text(pIconPath);

		pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_view_listwiththumbs_IN.png",NULL);
		g_value_set_string(&pColName, "LIST-VIEW");
		g_value_set_string(&pColIcon, pIconPath);
		g_value_set_string(&pColToolTip, "LIST");
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,1,DRAWER_COLUMN_NAME,&pColName);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,1,DRAWER_COLUMN_ICON,&pColIcon);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,1,DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);
		video_player_free_pkgdatadir_text(pIconPath);
		
		pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_view_details.png",NULL);
		g_value_set_string(&pColName, "DETAIL-VIEW");
		g_value_set_string(&pColIcon, pIconPath);
		g_value_set_string(&pColToolTip, "DETAIL");
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,2,DRAWER_COLUMN_NAME,&pColName);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,2,DRAWER_COLUMN_ICON,&pColIcon);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,2,DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);
		video_player_free_pkgdatadir_text(pIconPath);

	}
	else if(inView == LIST_VIEW)
	{
		pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_view_details.png",NULL);
		g_value_set_string(&pColName, "DETAIL-VIEW");
		g_value_set_string(&pColIcon, pIconPath);
		g_value_set_string(&pColToolTip, "DETAIL");
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,0,DRAWER_COLUMN_NAME,&pColName);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,0,DRAWER_COLUMN_ICON,&pColIcon);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,0,DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);
		video_player_free_pkgdatadir_text(pIconPath);
		
		pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_view_thumbs.png",NULL);
		g_value_set_string(&pColName, "THUMB-VIEW");
		g_value_set_string(&pColIcon, pIconPath);
		g_value_set_string(&pColToolTip, "THUMB");
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,1,DRAWER_COLUMN_NAME,&pColName);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,1,DRAWER_COLUMN_ICON,&pColIcon);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,1,DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);
		video_player_free_pkgdatadir_text(pIconPath);

		pIconPath = g_strconcat(PKG_DATA_DIRECTORY,"/icon_view_listwiththumbs_IN.png",NULL);
		g_value_set_string(&pColName, "LIST-VIEW");
		g_value_set_string(&pColIcon, pIconPath);
		g_value_set_string(&pColToolTip, "LIST");
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,2,DRAWER_COLUMN_NAME,&pColName);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,2,DRAWER_COLUMN_ICON,&pColIcon);
		thornbury_model_insert_value(defaultVideoPlayer->priv->pViewsDrawerModel,2,DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);
		video_player_free_pkgdatadir_text(pIconPath);
	}
}
