/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


/**********************************************************************
              				Header Files
 ***********************************************************************/
#include "eye.h"

extern ClutterActor *pVideoPlayerStage;
extern gchar *pURL;
extern EyeVideoPlayer *defaultVideoPlayer;
/**********************************************************************
              			Function Prototypes Start
 ***********************************************************************/
static void app_mgr_name_appeared (GDBusConnection *connection,const gchar *name,const gchar *name_owner,gpointer user_data);

static void app_mgr_name_vanished(GDBusConnection *connection,const gchar *name,gpointer user_data);


static void audio_mgr_proxy_clb( GObject *source_object, GAsyncResult *res, gpointer user_data);

static void eye_register_clb(GObject *source_object,GAsyncResult *res,	gpointer user_data);

static void new_app_status(CanterburyAppManager *object,const gchar *app_name,gint new_app_state,GVariant *arguments,gpointer user_data);

static void on_statusbar_name_appeared (GDBusConnection *connection,const gchar *name,const gchar *name_owner,gpointer user_data);

static void on_statusbar_name_vanished(GDBusConnection *connection,const gchar *name,gpointer user_data);
/**********************************************************************
              			Function Prototypes End
 ***********************************************************************/

static void video_player_statusbar_proxy_clb( GObject *source_object,
		GAsyncResult *res,
		gpointer user_data)
{
	EyeVideoPlayer *pVideoPlayer = user_data;
	GError *error = NULL;

	g_return_if_fail (EYE_IS_VIDEO_PLAYER (pVideoPlayer));
	pVideoPlayer->priv->status_bar_proxy=mildenhall_statusbar_proxy_new_finish (res,&error);
}

static void
on_statusbar_name_appeared (GDBusConnection *connection,
		const gchar     *name,
		const gchar     *name_owner,
		gpointer         user_data)
{
	mildenhall_statusbar_proxy_new (connection,
			G_DBUS_PROXY_FLAGS_NONE,
			"org.apertis.Mildenhall.Statusbar",
            "/org/apertis/Mildenhall/Statusbar",
			NULL,
			video_player_statusbar_proxy_clb,
			user_data);
}


static void
on_statusbar_name_vanished(GDBusConnection *connection,
		const gchar     *name,
		gpointer         user_data)
{
	EyeVideoPlayer *pVideoPlayer = user_data;

	g_return_if_fail (EYE_IS_VIDEO_PLAYER (pVideoPlayer));
	if(NULL != pVideoPlayer->priv->status_bar_proxy)
		g_object_unref(pVideoPlayer->priv->status_bar_proxy);

}





/*********************************************************************************************
 * Function: initialize_app_manager_client_handler
 * Description: Initializes the client handlers
 * Parameters:  actor
 * Return:   void
 ********************************************************************************************/
void initialize_app_manager_client_handler(ClutterActor* actor)
{

	g_bus_watch_name (G_BUS_TYPE_SESSION,
			"org.apertis.Canterbury",
			G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
			app_mgr_name_appeared,
            app_mgr_name_vanished,
			actor,
			NULL);

	g_bus_watch_name (G_BUS_TYPE_SESSION,
			"org.apertis.Mildenhall.Statusbar",
			G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
			on_statusbar_name_appeared,
			on_statusbar_name_vanished,
			actor,
			NULL);
}

static gboolean
call_thumb_item_activated (gpointer pUserData)
{
	eye_thumb_roller_item_activated_cb(NULL,GPOINTER_TO_INT(pUserData),defaultVideoPlayer);
	return FALSE;
}

static gboolean
thumb_set_focus (gpointer pUserData)
{

	thornbury_set_property(defaultVideoPlayer->priv->pThornburyViewManager, THUMB_VIEW_VIDEO_THUMB_ROLLER, "focused-row",GPOINTER_TO_INT(pUserData) , NULL);
	g_timeout_add(500,call_thumb_item_activated,pUserData);
//        eye_thumb_roller_item_activated_cb(NULL,GPOINTER_TO_INT(pUserData),defaultVideoPlayer);
	return FALSE;
}
static void
audio_mgr_proxy_clb( GObject *source_object,
		GAsyncResult *res,
		gpointer user_data)
{
	GError *error;
	EyeVideoPlayer *pVideoPlayer = EYE_VIDEO_PLAYER (user_data);
	/* finishes the proxy creation and gets the proxy ptr */
	pVideoPlayer->priv->audio_mgr_proxy = canterbury_audio_mgr_proxy_new_finish(res , &error);

	/* register for app manager signals */
	g_signal_connect( pVideoPlayer->priv->audio_mgr_proxy,
			"audio-response",
			G_CALLBACK (handle_audio_response),
			user_data);

	if(pVideoPlayer->priv->audio_mgr_proxy == NULL)
	{
		g_printerr("error %s \n",g_dbus_error_get_remote_error(error));
		return;
	}
	
	if(pURL != NULL)
        {
                guint index = 0;
                for( ; index < pVideoPlayer->priv->pVideoUriList->len ; index++)
                {
                        const gchar *searchURL = g_ptr_array_index (pVideoPlayer->priv->pVideoUriList,index);
                        if (g_strcmp0 (searchURL, pURL) == 0)
                        {
                                //Call the item activated cb with the index.
				g_timeout_add(2000,thumb_set_focus,GINT_TO_POINTER(index));
                                break;
                        }
                }
        }
        else
                g_print("\n VP:Not From Global search \n");

}

/*********************************************************************************************
 * Function: app_mgr_proxy_clb
 * Description: Proxy callback for AppManager
 * Parameters:  source_object,res,UserData
 * Return:   void
 ********************************************************************************************/

static void
app_mgr_proxy_clb( GObject *source_object,
		GAsyncResult *res,
		gpointer user_data)
{
	GError *error;
	EyeVideoPlayer *pVideoPlayer = EYE_VIDEO_PLAYER (user_data);
	/* finishes the proxy creation and gets the proxy ptr */
	pVideoPlayer->priv->app_mgr_proxy = canterbury_app_manager_proxy_new_finish(res , &error);

	if(pVideoPlayer->priv->app_mgr_proxy == NULL)
	{
		g_printerr("error %s \n",g_dbus_error_get_remote_error(error));
		return;
	}

	/* register for app manager signals */
	g_signal_connect (pVideoPlayer->priv->app_mgr_proxy,
			"new-app-state",
			G_CALLBACK (new_app_status),
			user_data);

	canterbury_app_manager_call_register_my_app(pVideoPlayer->priv->app_mgr_proxy,
		pVideoPlayer->priv->EYE_APP_NAME , 0 , NULL , eye_register_clb , user_data);
}

/*********************************************************************************************
 * Function: app_mgr_name_appeared
 * Description: Name appeared callback function for App Manager
 * Parameters:  connection,name,name_owner,userData
 * Return:   void
 ********************************************************************************************/

static void
app_mgr_name_appeared (GDBusConnection *connection,
		const gchar     *name,
		const gchar     *name_owner,
		gpointer         user_data)
{

	/* Asynchronously creates a proxy for the App manager D-Bus interface */
	canterbury_app_manager_proxy_new (
			connection,
			G_DBUS_PROXY_FLAGS_NONE,
			"org.apertis.Canterbury",
			"/org/apertis/Canterbury/AppManager",
			NULL,
			app_mgr_proxy_clb,
			user_data);

	canterbury_audio_mgr_proxy_new (
                        connection,
                        G_DBUS_PROXY_FLAGS_NONE,
                        "org.apertis.Canterbury",
                        "/org/apertis/Canterbury/AudioMgr",
                        NULL,
                        audio_mgr_proxy_clb,
                        user_data);

}

/*********************************************************************************************
 * Function: app_mgr_name_vanished
 * Description: Name vanished callback of appmanager
 * Parameters:  connection,name,userData
 * Return:   void
 ********************************************************************************************/

static void
app_mgr_name_vanished( GDBusConnection *connection,
		const gchar     *name,
		gpointer         user_data)
{
	EyeVideoPlayer *pVideoPlayer = EYE_VIDEO_PLAYER (user_data);
	if(NULL != pVideoPlayer->priv->app_mgr_proxy)
		g_object_unref(pVideoPlayer->priv->app_mgr_proxy);
}

static void video_player_remove_from_pdi(EyeVideoPlayer *pVideoPlayer)
{
	guint inNumRows = 0;
	GPtrArray *gpArray = NULL;
	gpArray = seaton_preference_get(pVideoPlayer->priv->sqliteObj, NULL, NULL,NULL);
	for (inNumRows = 0; inNumRows < gpArray->len; inNumRows++)
	{
		int inResult = seaton_preference_remove(pVideoPlayer->priv->sqliteObj,NULL, NULL,NULL);
		if(!inResult)
		{
			//	g_print("\n Removing from PDI is success \n");
		}
	}

	if(NULL != gpArray)
	{
		g_ptr_array_free(gpArray,TRUE);
		gpArray=NULL;
	}

}
/*********************************************************************************************
 * Function: new_app_status
 * Description: Callback from the AppManager to show the status of the application.
 * Parameters:  object ,appname,newAppState,arguments,userData
 * Return:   void
 ********************************************************************************************/

static void
new_app_status( CanterburyAppManager  *object,
		const gchar             *app_name,
		gint                     new_app_state,
		GVariant                *arguments,
		gpointer                 user_data)
{
	EyeVideoPlayer *pVideoPlayer = EYE_VIDEO_PLAYER (user_data);
	GPtrArray *gpArray ;
	GHashTable *hashChk=NULL;
	if( g_strcmp0(pVideoPlayer->priv->EYE_APP_NAME ,app_name ) == FALSE )
	{
		switch (new_app_state)
		{
		case CANTERBURY_APP_STATE_START:
		{
			if(pVideoPlayer->priv->bPlayState == TRUE)
			{
				gpArray = seaton_preference_get(pVideoPlayer->priv->sqliteObj,NULL, NULL,NULL);
				if(gpArray->len > 0)
				{
					gchar *curFile;

					hashChk = g_ptr_array_index(gpArray, 0);
					curFile = g_strdup (g_hash_table_lookup (hashChk, "CurrentPlayingFile"));
					pVideoPlayer->priv->currentPlayingFile = atoi(curFile); // take from pdi

					curFile= g_strdup(g_hash_table_lookup(hashChk,"ShuffleState"));
					pVideoPlayer->priv->isShuffle = atoi(curFile);

					curFile= g_strdup(g_hash_table_lookup(hashChk,"RepeatState"));
					pVideoPlayer->priv->isRepeat = atoi(curFile);

					curFile= g_strdup(g_hash_table_lookup(hashChk,"PlayState"));
					pVideoPlayer->priv->progressBarPlayState  = atoi(curFile);

					curFile= g_strdup(g_hash_table_lookup(hashChk,"PlayingDuration"));
					pVideoPlayer->priv->playingDuration = atof(curFile);

					video_player_remove_from_pdi(pVideoPlayer);

					video_player_last_user_mode_handler(pVideoPlayer);

				}
			}
			break;
		}


		case CANTERBURY_APP_STATE_BACKGROUND:
			/* see if it can do a clutter actor hide */
			break;

		case CANTERBURY_APP_STATE_SHOW:
			/* check if the application needs to resynchronize with its server */
			break;

		case CANTERBURY_APP_STATE_RESTART:
			/* check if the application needs to resynchronize with its server */
			break;

		case CANTERBURY_APP_STATE_OFF:
			/* store all persistence infornmation immediately.. */
			break;

		case CANTERBURY_APP_STATE_PAUSE:
			/* store all persistence infornmation immediately..*/
			break;

		default:
			g_printerr("hello world app in unknown state ???  \n" );
			break;
		}
	}
}
/*********************************************************************************************
 * Function: eye_register_clb
 * Description: Registers the VideoPlayer Application to app manager.
 * Parameters:  source_object , res, userData.
 * Return:   void
 ********************************************************************************************/

static void
eye_register_clb( GObject *source_object,
		GAsyncResult *res,
		gpointer user_data)
{
	EyeVideoPlayer *pVideoPlayer = EYE_VIDEO_PLAYER (user_data);
	gboolean return_val;
	GError *error = NULL;

	return_val = canterbury_app_manager_call_register_my_app_finish(pVideoPlayer->priv->app_mgr_proxy , res , &error);

	if(return_val == FALSE)
	{
		gchar* error_msg = g_dbus_error_get_remote_error(error);
		g_printerr("error %s \n",error_msg);
		g_free(error_msg);

		g_dbus_error_strip_remote_error(error);
		g_printerr("error %s \n",error->message);
		return;
	}
}
/**********************************************************************
              				END OF FILE
 ***********************************************************************/
