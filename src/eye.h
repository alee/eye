/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* inclusion guard */
#ifndef __EYE_H__
#define __EYE_H__

#include <clutter-gst/clutter-gst.h>
#include <stdlib.h>
#include <clutter/clutter.h>

#include <canterbury/gdbus/canterbury.h>
#include "barkway.h"
#include <barkway-enums.h>
#include "libgrassmoor-tracker.h"

#include <thornbury/thornbury.h>
#include <mildenhall/mildenhall.h>

#include <libgrassmoor-av-player.h>

#include <seaton-preference.h>
#include <stdlib.h>

G_BEGIN_DECLS

#define EYE_BUNDLE_ID "org.apertis.Eye"
/* There is only one app in this bundle, so we use the same string for both. */
#define EYE_APP_ID EYE_BUNDLE_ID

#define EYE_TYPE_VIDEO_PLAYER eye_get_type()

#define EYE_VIDEO_PLAYER(obj) \
		(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
				EYE_TYPE_VIDEO_PLAYER, EyeVideoPlayer))

#define EYE_VIDE_PLAYER_CLASS(klass) \
		(G_TYPE_CHECK_CLASS_CAST ((klass), \
				EYE_TYPE_VIDEO_PLAYER, EyeVideoPlayerClass))

#define EYE_IS_VIDEO_PLAYER(obj) \
		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
				EYE_TYPE_VIDEO_PLAYER))

#define EYE_IS_VIDEO_PLAYER_CLASS(klass) \
		(G_TYPE_CHECK_CLASS_TYPE ((klass), \
				EYE_TYPE_VIDEO_PLAYER))

#define EYE_VIDEO_PLAYER_GET_CLASS(obj) \
		(G_TYPE_INSTANCE_GET_CLASS ((obj), \
				EYE_TYPE_VIDEO_PLAYER, EyeVideoPlayerClass))

#define SEARCH "search"
#define DELETE          "delete"
#define TO_PLAYLIST     "to-playlist"
#define FAVOURITE     "favourite"
#define RECENT           "recent"
#define RELATED         "related"

#define THUMB_VIEW_VIDEO_THUMB_ROLLER "ThumbRoller"
#define THUMB_VIEW_SORT_ROLLER 		  "ThumbSortRoller"
#define THUMB_VIEWS_DRAWER            "ThumbViewsDrawer"
#define THUMB_CONTEXT_DRAWER          "ThumbContextDrawer"

#define LIST_VIEW_LIST_ROLLER		  "ListRoller"

#define DETAIL_CONTEXT_DRAWER	"DetailContextDrawer"
#define DETAIL_META_ROLLER	"MetaRoller"
#define DETAIL_BOTTOMBAR	"BottomBar"
#define DETAIL_INFO_ROLLER	"InfoRoller"
#define DETAIL_PROGRESSBAR	"ProgressBar"

#define SHUFFLE		"shuffle"
#define REPEAT		"repeat"

#define FULL_SCREEN_VIEW_SWITCH_DRAWER "FullScreenViewSwitch"
#define FULL_SCREEN_ASPECT_RATIO_DRAWER "FullScreenAspectRatio"
#define ASPECT_RATIO "AspectRatio"
#define VIEW_SWITCH "ViewSwitch"
typedef struct _EyeVideoPlayer        EyeVideoPlayer;
typedef struct _EyeVideoPlayerClass    EyeVideoPlayerClass;
typedef struct _EyeVideoPlayerPrivate  EyeVideoPlayerPrivate;

struct _EyeVideoPlayer
{
	ClutterActor parent;
	EyeVideoPlayerPrivate *priv;
};

struct _EyeVideoPlayerClass
{
	ClutterActorClass parent_class;
};
struct _EyeVideoPlayerPrivate
{
	CanterburyAppManager *app_mgr_proxy;
	CanterburyAudioMgr *audio_mgr_proxy;
	MildenhallStatusbar *status_bar_proxy;
	GrassmoorTracker *tracker;

	GPtrArray *pVideoUriList;
	GPtrArray *pVideoInfoList;

	ThornburyModel *pThumbRollerModel;
	GHashTable *pThumbModelHash;

	ThornburyModel *pSortRollerModel;
	ThornburyModel *pViewsDrawerModel;
	ThornburyModel *pContextDrawerModel;

	ThornburyModel *pListRollerModel;
	GHashTable *pListModelHash;

	ThornburyModel *pInfoHeaderModel;
	ThornburyModel *pMetaRollerModel;
	ThornburyModel *pBottomBarModel;
	GHashTable *pDetailModelHash;
	
	GHashTable *pFullScreenModelHash;
	ThornburyViewManager *pThornburyViewManager;

	GPtrArray *pPhotoInfoResultPointer;
	gint fileToUpdate;
	gint prevPlayingFile;
	
	GrassmoorAVPlayer *player;

	gboolean seekStart;

	gint inDisconnect;

	gint currentPlayingFile;
	gboolean isShuffle;
	gboolean bPlayState;
	gboolean isRepeat;
	
	GHashTable *pExitStateHash;
	SeatonPreference *sqliteObj;
	
	gchar *pHeaderText;
	gint rowNumber;
	gdouble playingDuration;
	gboolean progressBarPlayState;
	gint pCurrentView;
	gchar *EYE_APP_NAME;
	
  	GString *pPopThumbPath;
	gchar *pPopFileBaseName;
	guint pExtraThumbs;
	guint inRequestCount;
	gboolean bRemoveState;
	guint inThumbsToUpdate;
};

enum
{
	THUMB_VIEW,
	LIST_VIEW,
	DETAIL_VIEW,
	FULL_SCREEN_VIEW
};

typedef enum
{

	EYE_VIDEOPLAYER_DEBUG          = 1 << 0,

} VideoPlayerFlag;
#define VIDEOPLAYER_HAS_DEBUG               ((videoplayer_debug_flags ) & 1)
#define EYE_VIDEOPLAYER_DEBUG( a ...) \
		if (G_LIKELY (VIDEOPLAYER_HAS_DEBUG )) \
		{			            	\
			g_print(a);		\
		}

extern guint  videoplayer_debug_flags ;

static const GDebugKey videoplayer_debug_keys[] =
{

		{ "videoplayer",	EYE_VIDEOPLAYER_DEBUG }
};



GType eye_get_type (void) G_GNUC_CONST;

/**********************************************************************
              		Function Prototypes
 ***********************************************************************/

ClutterActor *eye_create (const gchar *app_id);

void eye_init_ui(EyeVideoPlayer *pVideoPlayer);
void initialize_app_manager_client_handler(ClutterActor* actor);
void extract_video_info_from_meta_tracker(EyeVideoPlayer *pVideoPlayer);
void eye_view_switched(ThornburyViewManager *pThornburyViewManager,gpointer pName,gpointer pUserData);
void eye_thumb_roller_item_activated_cb (MildenhallRollerContainer *pRoller, guint inRow, gpointer pUserData);
void eye_list_roller_item_activated_cb (MildenhallRollerContainer *pRoller, guint inRow, gpointer pUserData);

void eye_views_drawer_button_released_cb(ClutterActor *pButton, gchar *pName, gpointer pUserData);
void eye_thumb_context_drawer_button_released_cb(ClutterActor *pButton, gchar *pName, gpointer pUserData);

void eye_populate_list_roller_items(EyeVideoPlayer *pVideoPlayer,GrassmoorMediaInfo* MedStructVideoInfo,GString *pThumbnailPath,gchar *pFileBaseName);
void eye_populate_thumb_roller_items(GString *pThumbnailPath,GrassmoorMediaInfo* MedStructVideoInfo,gchar *pFileBaseName);
void thumbnail_generated_cb(const gchar** pFileName);

void update_video_player_progress_and_bottom_bar(gdouble duration);
gboolean progress_bar_seek_end_cb(MildenhallProgressBar *progressBar, gfloat position, gpointer userData);
gboolean progress_bar_seek_started_cb(MildenhallProgressBar *progressBar, gfloat position, gpointer userData);
gboolean progress_bar_pause_requested_cb(MildenhallProgressBar *progressBar, gpointer userData);
gboolean progress_bar_play_requested_cb(MildenhallProgressBar *progressBar, gpointer userData);
gboolean progress_bar_seek_updated_cb(MildenhallProgressBar *progressBar, gpointer userData);

gboolean bottom_bar_pressed(MildenhallBottomBar *bottomBar, gchar *pButtonName, gpointer pUserData);
gboolean bottom_bar_released(MildenhallBottomBar *bottomBar, gchar *pButtonName, gpointer pUserData);
void update_detail_view_header(gchar *pFileName);
void handle_audio_response( CanterburyAudioMgr *object, const gchar *arg_app_name, guint arg_audio_result , gpointer user_data);
void media_end_of_stream(GObject *pObject, GParamSpec *pspec,gpointer data);
void set_video_player_progress_bar(GObject *pObject, GParamSpec *pspec,gpointer data);
void video_player_create_exit_state_pdi(EyeVideoPlayer *pVideoPlayer);
void update_detail_meta_roller(GrassmoorMediaInfo *MedStruct);

ClutterContent *video_player_create_texture_content(gchar *pFilePath, gint width, gint height);
void add_items_to_thumb_roller (void);
void video_player_last_user_mode_handler(EyeVideoPlayer *pVideoPlayer);

void eye_full_screen_left_context_drawer_button_released_cb(ClutterActor *pButton, gchar *pName, gpointer pUserData);
void eye_full_screen_right_context_drawer_button_released_cb(ClutterActor *pButton, gchar *pName, gpointer pUserData);

void eye_view_switch_begin_cb(ThornburyViewManager *pThornburyViewManager, gchar *pName, gpointer *pUserData);
void add_remove_thumbs_divisible_by_4(gboolean bAddThumbs);
void update_views_drawer(gint inView);
gboolean eye_update_thumb_list(gpointer pUserData);
gboolean eye_update_list_roller(gpointer pUserData);
G_END_DECLS

#endif /* __VideoPlayer_H__ */
