eye (0.2020.1) apertis; urgency=medium

  * Switch to native format to work with the GitLab-to-OBS pipeline.
  * gitlab-ci: Link to the Apertis GitLab CI pipeline definition.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Tue, 04 Feb 2020 16:28:39 +0800

eye (0.1706.1-0co1) 17.06; urgency=medium

  * Stop using deprecated transitional package canterbury-dev

 -- Simon McVittie <smcv@collabora.com>  Fri, 21 Apr 2017 19:36:58 +0100

eye (0.1706.0-0co1) 17.06; urgency=medium

  * Debian packaging fixes (Apertis: T3928):
    - d/rules: normalize whitespace
    - d/rules: factor out ${BUNDLE_ID}, ${prefix}
    - d/rules: use GNU-style libdir, libexecdir for built-in app-bundle
    - Don't build static libraries for built-in app-bundle
    - d/rules: don't pass --ignore-missing-info to dpkg-shlibdeps
    - d/rules: fail if files are installed but not packaged
    - Normalize packaging (wrap-and-sort -abst)
    - Remove manually-maintained debug symbols package
    - eye.postinst: remove
    - Move to debhelper compat level 10
    - Use dh-exec to substitute BUNDLE_ID into packaging files
    - debian/gbp.conf: Add Apertis settings for git-buildpackage
    - debian/.gitignore: Ignore debhelper-build-stamp
    - Do not explicitly run dh-autoreconf, done by default in compat level 10

 -- Simon McVittie <smcv@collabora.com>  Fri, 21 Apr 2017 15:06:57 +0100

eye (0.1703.0-0co1) 17.03; urgency=medium

  [ Justin Kim ]
  * Use common headers of mildenhall and thornbury

  [ Simon McVittie ]
  * Add debian/source/apertis-component marking this as a HMI package


 -- Justin Kim <justin.kim@collabora.com>  Fri, 10 Feb 2017 16:47:27 +0900

eye (0.5.1-0co1) 16.09; urgency=medium

  [ Thushara Malali Somesha ]
  * Add AM_SILENT_RULES
  * Remove including gst/gst.h header file
  * Use generic GNOME autogen script
  * Remove unused code and debug prints

  [ Nandini Raju ]
  * Space indentation comment added in all source files
  * Unwanted comments removed from code

 -- Héctor Orón Martínez <hector.oron@collabora.co.uk>  Thu, 15 Sep 2016 22:45:06 +0200

eye (0.5.0-0co1) 16.06; urgency=medium

  [ Justin Kim ]
  * bump mildenhall dependency version
  * update APIs for mildenall g-ir support changes (Apertis: T1602)

  [ Abhiruchi Gupta ]
  * Progressbar property names updated.

 -- Justin Kim <justin.kim@collabora.com>  Tue, 17 May 2016 22:21:53 +0900

eye (0.4.4-0co1) 16.03; urgency=medium

  [ Abhiruchi Gupta ]
  * Roller focus issue resolved for thumb view.

  [ Sjoerd Simons ]
  * Release 0.4.4

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Thu, 14 Apr 2016 10:16:27 +0200

eye (0.4.3-0co1) 16.03; urgency=medium

  [ Philip Withnall ]
  * debian: Add abstractions/tracker-clients to the AppArmor profile

  [ Arikkala Vijeth ]
  * Added creative commons and license files to eye.

  [ Sjoerd Simons ]
  * Add CC-BY-SA 4.0 license for the images

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Fri, 04 Mar 2016 09:42:43 +0100

eye (0.4.2-0co1) 16.03; urgency=medium

  [ Guillaume Desmottes ]
  * arcconfig: add default-reviewers
  * fix PKG_DATA_DIRECTORY
  * migrate to new clapton-log API
  * port to new libgrassmoor API
  * no need to dup strings passed to g_object_set()

  [ Simon McVittie ]
  * Stop calling into libclapton altogether

  [ Sjoerd Simons ]
  * debian: bump libgrassmoor-dev b-d

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Tue, 01 Mar 2016 12:36:35 +0100

eye (0.4.1-0co1) 15.12; urgency=medium

  * .arcconfig: add
  * AppArmor profile: include <tunables/global>, for @{HOME}
  * Stop installing GSchema (T431)
  * Use AX_COMPILER_FLAGS instead of our own version
  * Fix -Wdeclaration-after-statement warnings
  * Ignore incorrect declarations in other modules
  * Make functions static where possible
  * Remove a duplicate declaration
  * Fix some gint/guint signedness agreement
  * Be a bit more const-correct
  * Use correct declarations for functions with no arguments
  * Don't use G_LIKELY where it doesn't help
  * Remove executable flag from non-scripts
  * Consistently refer to EyeVideoPlayer type under that name
  * Change app ID and bundle ID to org.apertis.Eye (T522)
  * Use a Seaton database in a directory named for the bundle

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Fri, 23 Oct 2015 16:28:23 +0100

eye (0.4.0-0co1) 15.12; urgency=medium

  [ Philip Withnall ]
  * Fix thumbnail cache directory in AppArmor profile

  [ Simon McVittie ]
  * Include symbolic links from our schemata into the system directory,
    and rely on dpkg triggers to update the cache, instead of copying them
    in postinst and updating the cache manually. This incidentally fixes the
    bug that #POSTINST# was missing from debian/postinst and so
    any debhelper-created postinst snippets would not work.
  * Update AppArmor profile (part of T365):
    - include <abstractions/chaiwala-base> for basic things
    - include <abstractions/fonts> for UI fonts
    - use more generic permissions for the app's own files
    - allow loading Mildenhall themes
    - do not allow executing an unconfined shell
    - do not specifically allow reading /sys/devices/system/cpu/,
      it is implied by the chaiwala-base abstraction
  * Add a .desktop file describing the app
  * Symlink our icon into an icon theme directory
  * autogen.sh: respect NOCONFIGURE
  * Use a static README and the standard Autotools INSTALL file
  * debian/.gitignore: add

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Mon, 19 Oct 2015 19:26:24 +0100

eye (0.3.99-0co1) 15.09; urgency=medium

  [ Sjoerd Simons ]
  * Remove the dependency on clutter-x11

  [ Simon McVittie ]
  * debian/source/options: ignore git.mk and .arcconfig in the Debian
    diff, since these are not intended to appear in tarballs but may
    appear in git

  [ Sjoerd Simons ]
  * debian/source/options: Also ignore ChangeLog as it's generated on 
    make dist

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Mon, 22 Jun 2015 17:18:40 +0200

eye (0.3-0rb1) 15.03; urgency=medium

  * Initial import

 -- Apertis package maintainers <packagers@lists.apertis.org>  Tue, 10 Mar 2015 06:27:08 +0000
